﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Speech.Synthesis;
using System.Speech.Recognition;

class Program
{
    static SpeechSynthesizer synth;
    static SpeechRecognitionEngine speech;

    static void Main(string[] args)
    {
        // initialize speech synth
        synth = new SpeechSynthesizer();
        synth.SetOutputToDefaultAudioDevice();

        synth.Speak("Greetings, what would you like to buy?");
    }
        /*
        // initialize speech recognition engine
        speech = new SpeechRecognitionEngine();
        speech.SetInputToDefaultAudioDevice();
        speech.LoadGrammar(new DictationGrammar());

        // add event handler
        speech.SpeechRecognized += SpeechFound;

        // run forever
        while (true) {
            speech.Recognize();
        }
    }

    /**
     * Speech handling function used by seperate thread for async detection.
     *//*
    private static void SpeechFound(object sender, SpeechRecognizedEventArgs e)
    {
        // get message from speech recognizer
        string message = e.Result.Text;
        float confidence = e.Result.Confidence;

        // send to console
        Console.WriteLine("{0} : {1}", confidence, message);

        // TODO
        // Add Neural Network or Logistic Regression Chat Bot.
        // We can create a chatbot for NPC characters like a parsnip merchant.

        // send to synth
        synth.SpeakAsync(message);
    }*/
}
