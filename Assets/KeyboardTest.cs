﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("k"))
        {
            foreach (EnemyHealth h in GameObject.FindObjectsOfType<EnemyHealth>())
            {
                h.Damage(1000);
            }
        }
	}
}
