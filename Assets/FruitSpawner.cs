﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitSpawner : MonoBehaviour {
    public RpgItem[] Fruits;
    public float SpawnTime = 20;

    float Timer;
	// Use this for initialization
	void Start () {
        Timer = SpawnTime;
	}
	
	// Update is called once per frame
	void Update () {
        if (Timer < 0) {
            Timer = SpawnTime;

            SpawnFruit();
        }
        Timer -= Time.deltaTime;
	}

    void SpawnFruit()
    {
        GameObject pickup = Instantiate(Resources.Load("PickableItem"), transform.position, transform.rotation) as GameObject;
        RpgPickable pickable = pickup.GetComponent<RpgPickable>();

        int choice = UnityEngine.Random.Range(0, Fruits.Length);
        RpgItem fruit = Fruits[choice];
        pickable.Initialize(fruit);
    }
}
