﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour {

    public float DamageAmount = 5;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) {
            PlayerHealth ph = other.GetComponent<PlayerHealth>();
            ph.DamagePlayer(DamageAmount * Time.deltaTime);
        }
    }
}
