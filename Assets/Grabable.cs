﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabable : MonoBehaviour {

    EnemyAnimation ea;
    EnemyMovement em;
    EnemyHealth eh;

    // Use this for initialization
    void Start () {
        ea = GetComponent<EnemyAnimation>();
        em = GetComponent<EnemyMovement>();
        eh = GetComponent<EnemyHealth>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Grab ()
    {
        if (ea)
            ea.enabled = false;
        if (em)
            em.enabled = false;
        if (eh)
            eh.Damage(1000);

        foreach (Rigidbody rb in GetComponentsInChildren<Rigidbody>())
        {
            rb.velocity = new Vector3();
            rb.angularVelocity = new Vector3();
        }
    }
}
