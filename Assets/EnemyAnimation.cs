﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Animator))]
public class EnemyAnimation : MonoBehaviour {

    NavMeshAgent agent;
    Animator animator;

    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        // pass agent velocity to the animation system.
        Vector3 currentVelocity = agent.velocity;
        float dx = Vector3.Dot(transform.forward, currentVelocity);
        float dy = Vector3.Dot(transform.right, currentVelocity);
        animator.SetFloat("Strafe", dy);
        animator.SetFloat("Forward", dx);
	}
    
    void OnDisable()
    {
        animator.enabled = false;
    }
}
