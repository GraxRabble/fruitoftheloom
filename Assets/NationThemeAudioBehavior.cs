﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NationThemeAudioBehavior : MonoBehaviour {

    public AudioClip nationTheme;

	// Use this for initialization
	void Start () {
        GetComponent<AudioSource>().playOnAwake = false;
        GetComponent<AudioSource>().clip = nationTheme;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider collision)
    {
        GetComponent<AudioSource>().Play();
    }

    private void OnTriggerExit(Collider collision)
    {
        GetComponent<AudioSource>().Stop();
    }
}
