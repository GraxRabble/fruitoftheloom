﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PatrolPointList", menuName = "Data/PatrolPointList")]
public class PatrolPointList : ScriptableObject {
    public Transform[] PatrolPoints;
}
