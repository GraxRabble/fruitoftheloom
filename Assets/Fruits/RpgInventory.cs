﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Component used to store items.
 */
public class RpgInventory : MonoBehaviour {

    public List<RpgItem> items;
    AudioSource sound;

	// Use this for initialization
	void Start () {
        items = new List<RpgItem>();

        sound = GetComponent<AudioSource>();
	}

    /**
     * Add an item to the inventory
     */
    public void AddItem(RpgItem item)
    {
        items.Add(item);

        if (sound)
            sound.PlayOneShot(item.PickupSound);

        PlayerHealth ph = GetComponent<PlayerHealth>();
        if (ph) {
            ph.DamagePlayer(-10);
        }
    }

    /**
     * All items are dropped on death.
     */
    public void DropItems()
    {
        foreach (RpgItem item in items)
        {
            GameObject pickup = Instantiate(Resources.Load("PickableItem"), transform.position, transform.rotation) as GameObject;
            RpgPickable pickable = pickup.GetComponent<RpgPickable>();
            pickable.Initialize(item);
        }
    }
}
