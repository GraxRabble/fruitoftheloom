﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Base class for any pickable and droppable item rendered as sprites
 * 
 * Place all food, potions, ammo, gold here
 */
[CreateAssetMenu(fileName = "RpgItem", menuName = "Rpg/Item")]
public class RpgItem : ScriptableObject {
    public string ItemName;
    public Sprite Sprite;
    public AudioClip PickupSound;
}
