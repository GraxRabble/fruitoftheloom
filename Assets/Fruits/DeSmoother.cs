﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Implements Double Exponential Smoothing
 */
public class DeSmoother
{
    int state;
    float sx, sy, sz = 0;
    float bx, by, bz = 0;
    float alpha = 0.1f, beta = 0.1f;

    public Vector3 get(Vector3 input)
    {
        Vector3 output = new Vector3();

        output.x = iteration(input.x, ref sx, ref bx);
        output.y = iteration(input.y, ref sy, ref by);
        output.z = iteration(input.z, ref sz, ref bz);
        return output;
    }

    private float iteration(float x, ref float s, ref float b)
    {
        if (state == 0)
        {
            s = x;
        } else if (state == 1)
        {
            b = x - s;
            s = x;
        } else
        {
            float s_old = s;
            s = x * alpha + (1 - alpha) * (s + b);
            b = (s - s_old) * beta + (1 - beta) * b;
        }
        state++;
        return s + b * 3;
    }
}
