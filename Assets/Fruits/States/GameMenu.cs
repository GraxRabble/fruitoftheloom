﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMenu : State
{
    GameManager game;

    // list of weapon cubes for the menu
    public List<GameObject> WeaponMenuCubes;

    // weapon cube object
    public GameObject weaponCube;

    // menu tools
    ToolMenuList menuList;

    public GameMenu(StateMachine manager, GameManager game) : base(manager)
    {
        this.game = game;

        // allocate menu list
        WeaponMenuCubes = new List<GameObject>();
        weaponCube = game.weaponCube;
        menuList = game.menuList;
    }

    public override void Enter()
    {
        game.textbox.text = "Menu";

        // switch what ever crazy weapon to the default fists for menu selection
        game.ToolManager.ChangeTool(game.InitialTool);


        Vector3 CameraPosition = Camera.main.transform.position;

        // initialize menu
        for (int i = 0; i < menuList.tools.Count; i++)
        {
            GameObject cube = GameObject.Instantiate(weaponCube, CameraPosition + game.transform.TransformVector(menuList.offsets[i], 0, 4), Quaternion.identity);
            cube.transform.parent = game.transform;
            WeaponMenuCube wmc = cube.GetComponent<WeaponMenuCube>();
            wmc.menuCallback = WeaponCallback;
            wmc.Initialize(menuList.tools[i]);
            WeaponMenuCubes.Add(cube);
        }

        // go into slow motion mode inside of the menu
        Time.timeScale = 0.1f;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }

    public override void Exit()
    {
        game.textbox.text = "";

        // destroy all cubes
        foreach (GameObject cube in WeaponMenuCubes)
        {
            GameObject.Destroy(cube);
        }
        WeaponMenuCubes.Clear();

        // set time back to normal
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }

    public override void Loop()
    {
        game.PollJoints();
        if (!game.KinectMan.IsUserDetected())
        {
            NextState(new GamePause(manager, game));
        }
        game.ToolManager.Loop();
        game.NormalFeet();
    }

    /**
     * Callback that is sent to WeaponMenuCube
     * If something hits the cube, the Menu will call this function and change weapons.
     */
    private void WeaponCallback(Tool tool)
    {
        game.ToolManager.ChangeTool(tool);
        NextState(new GamePlay(manager, game));
    }
}
