﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCrouch : State {
    GameManager game;
    Vector3 cameraOriginalPosition;
    float colliderOriginalHeight;

    public GameCrouch(StateMachine manager, GameManager game) : base(manager)
    {
        this.game = game;
    }

    public override void Enter()
    {
        game.textbox.text = "Crouch";

        // lower camera
        cameraOriginalPosition = Camera.main.transform.localPosition;
        Camera.main.transform.localPosition /= 3;

        // lower collider
        CapsuleCollider collider = game.GetComponent<CapsuleCollider>();
        colliderOriginalHeight = collider.height;
        collider.height /= 3;
    }

    public override void Exit()
    {
        game.textbox.text = "";

        // raise camera back to original height
        Camera.main.transform.localPosition = cameraOriginalPosition;

        // raise collider back to original height
        CapsuleCollider collider = game.GetComponent<CapsuleCollider>();
        collider.height = colliderOriginalHeight;
    }

    public override void Loop()
    {
        game.PollJoints();

        if (!game.KinectMan.IsUserDetected())
        {
            NextState(new GamePause(manager, game));
        }

        if (!game.DetectCrouch())
        {
            NextState(new GamePlay(manager, game));
        } else
        {
            game.ToolManager.Loop();
            game.NormalFeet();
            game.LeaningMovement();
        }
    }
}
