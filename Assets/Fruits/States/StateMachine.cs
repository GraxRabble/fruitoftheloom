﻿/**
 * Implements a state machine which switches between a series of states.
 * Great for handling menus and detecting gestures.
 */
public class StateMachine
{
    // current state of the state machine
    public State CurrentState { get; private set; }

    /**
     * Initialize the state machine with the following state.
     */
    public void InitialState(State initialState)
    {
        CurrentState = initialState;
        // Enter the initial state
        CurrentState.Enter();
    }

    /**
     * Execute the current state once per frame/loop
     */
    public void Loop()
    {
        CurrentState.Loop();
    }

    /**
     * Change the current state, executing the correspoding exit and enter methods.
     */
    public void ChangeState(State newState)
    {
        CurrentState.Exit();
        CurrentState = newState;
        CurrentState.Enter();
    }
}

/**
 * State class which will be the building block of a finite state machine.
 */
public abstract class State {
    // reference to state machine which stores the current state machine.
    public StateMachine manager { get; private set; }

    // initialize this state with a state manager
    // Must be called by base classes.
    public State(StateMachine manager)
    {
        this.manager = manager;
    }

    // change the state
    protected void NextState(State newState)
    {
        manager.ChangeState(newState);
    }

    /**
     * Executed once at the beginning of the state.
     * This is a good spot to initialize weapons/graphics.
     */
    public abstract void Enter();

    /**
     * Executed on every frame/loop.
     * This is a good spot to detect gestures.
     */
    public abstract void Loop();

    /**
     * Executed once at the end of the state.
     * This is a good spot to deallocate weapons/graphics.
     */
    public abstract void Exit();
}
