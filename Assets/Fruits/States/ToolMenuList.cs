﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Store menu data for GameMenu object.
 */
[CreateAssetMenu(fileName="ToolMenuList", menuName="Data/ToolMenuList")]
public class ToolMenuList : ScriptableObject {
    public List<Tool> tools;
    public List<float> offsets;
}
