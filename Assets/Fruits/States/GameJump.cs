﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameJump : State {
    GameManager game;
    Vector3 cameraOriginalPosition;
    float colliderOriginalHeight;
    float groundedCooldown;

    public GameJump(StateMachine manager, GameManager game) : base(manager)
    {
        this.game = game;
    }

    public override void Enter()
    {
        game.textbox.text = "Jump";

        game.PlayerRBody.velocity += game.transform.TransformVector(game.DeltaGroin.normalized) * game.JumpStrength;
        groundedCooldown = 1;
    }

    public override void Exit()
    {
        game.textbox.text = "";
    }

    public override void Loop()
    {
        game.PollJoints();
        if (!game.KinectMan.IsUserDetected())
        {
            NextState(new GamePause(manager, game));
        }
        game.ToolManager.Loop();
        game.NormalFeet();
        game.LeaningMovement();

        // guarentee one second of jump
        if (groundedCooldown > 0)
        {
            groundedCooldown -= Time.deltaTime;
        }

        // exit jump state when we land on something
        bool grounded = Physics.SphereCast(new Ray(game.transform.position, new Vector3(0, -1, 0)), 0.1f, 2);
        if (grounded)
        {
            NextState(new GamePlay(manager, game));
        }
    }
}
