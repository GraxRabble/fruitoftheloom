﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlay : State
{
    GameManager game;

    public GamePlay(StateMachine manager, GameManager game) : base(manager)
    {
        this.game = game;
    }

    public override void Enter()
    {
    }

    public override void Exit()
    {
    }

    public override void Loop()
    {
        game.PollJoints();

        // if detect toolbelt, go into menu
        if (!game.KinectMan.IsUserDetected())
        {
            NextState(new GamePause(manager, game));
        }
        else if (game.DetectToolbelt())
        {
            NextState(new GameMenu(manager, game));
        }
        else if (game.DetectFly())
        {
            NextState(new GameFly(manager, game));
        }
        else if (game.DetectCrouch())
        {
            NextState(new GameCrouch(manager, game));
        }
        else if (game.DetectJump())
        {
            NextState(new GameJump(manager, game));
        }
        else
        {
            game.ToolManager.Loop();
            game.NormalFeet();
            game.LeaningMovement();
        }
    }
}
