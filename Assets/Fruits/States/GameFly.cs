﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFly : State
{
    GameManager game;

    public float FlightTime = 60f;
    float groundedCooldown = 3f;

    public GameFly(StateMachine manager, GameManager game)
        : base(manager)
    {
        this.game = game;
    }

    public override void Enter()
    {
        game.textbox.text = "Fly";

        // switch what ever crazy weapon to the default fists for flying
        game.ToolManager.ChangeTool(game.InitialTool);

        // disable gravity
        game.GetComponent<Rigidbody>().useGravity = false;
    }

    public override void Exit()
    {
        game.textbox.text = "";

        // enable gravity
        game.GetComponent<Rigidbody>().useGravity = true;

        Vector3 angles = game.transform.rotation.eulerAngles;
        game.transform.rotation = Quaternion.Euler(0, angles.y, 0);
    }

    /**
     * We have limited amount of flight time
     */
    public override void Loop()
    {
        game.PollJoints();
        if (!game.KinectMan.IsUserDetected())
        {
            NextState(new GamePause(manager, game));
        }
        game.ToolManager.Loop();
        game.NormalFeet();
        game.FlyingMovement();

        FlightTime -= Time.deltaTime;

        // guarentee one second of jump
        if (groundedCooldown > 0)
        {
            groundedCooldown -= Time.deltaTime;
            return;
        }

        // stop if player touches ground or time runs out
        bool timeout = FlightTime < 0;
        bool grounded = Physics.SphereCast(new Ray(game.transform.position, new Vector3(0, -1, 0)), 0.1f, 2);
        if (timeout || grounded)
            manager.ChangeState(new GamePlay(manager, game));
    }
}
