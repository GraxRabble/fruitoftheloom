﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePause : State
{
    GameManager game;
    State previousState;

    public GamePause(StateMachine manager, GameManager game) : base(manager)
    {
        previousState = manager.CurrentState;
        this.game = game;
    }

    public override void Enter()
    {
        game.textbox.text = "Paused";
        
        // pause time
        Time.timeScale = 0;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }

    public override void Exit()
    {
        game.textbox.text = "";

        // restore normal time
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }

    public override void Loop()
    {
        // if user is detected, exit pause mode
        if (game.KinectMan.IsUserDetected())
        {
            NextState(previousState);
        }
    }
}
