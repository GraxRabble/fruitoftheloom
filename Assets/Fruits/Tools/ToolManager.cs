﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


/**
 * Handles switching between tools in the game.
 * 
 * The difference between StateMachine and ToolManager is that States can
 * change to different states on their own while Tools do not.
 */
public class ToolManager {
    private Tool CurrentTool;

    /**
     * Initialize ToolManager with a specific Tool
     */
    public ToolManager(Tool InitialTool)
    {
        CurrentTool = InitialTool;
        CurrentTool.Add();
    }

    /**
     * Run update on every frame for the current tool.
     */
    public void Loop()
    {
        CurrentTool.Loop();
    }

    /**
     * Change the current tool to a different tool, Handles initialization and cleanup automatically.
     */
    public void ChangeTool(Tool NewTool)
    {
        CurrentTool.Remove();
        CurrentTool = NewTool;
        CurrentTool.Add();
    }
}

public abstract class Tool : ScriptableObject {
    // prefab which represent the current tool
    public GameObject LeftHandAvatar;
    public GameObject RightHandAvatar;

    // Sprite which represent the tool, for the menu
    public Sprite Sprite;

    // reference to game manager
    protected GameManager game;
    protected GameObject LeftHandObject;
    protected GameObject RightHandObject;

    /**
     * Initialize tool's game objects.
     */
    public virtual void Add()
    {
        game = GameObject.FindObjectOfType<GameManager>();
        LeftHandObject = GameObject.Instantiate(LeftHandAvatar);
        RightHandObject = GameObject.Instantiate(RightHandAvatar);

        LeftHandObject.transform.parent = game.transform;
        RightHandObject.transform.parent = game.transform;
    }

    /**
     * Clean up tool's game objects.
     */
    public virtual void Remove()
    {
        GameObject.Destroy(LeftHandObject);
        GameObject.Destroy(RightHandObject);
    }

    /**
     * Execute on every loop, update tool animation.
     */
    public virtual void Loop()
    {
        Vector3 CameraPosition = Camera.main.transform.position;

        GameManager.BodyState Body = game.Body;
        float armScale = game.armScale;
        Vector3 bodyOffset = game.bodyOffset;

        // normal arms
        Vector3 leftHandDelta = Body.LeftHand - Body.LeftShoulder;
        Vector3 rightHandDelta = Body.RightHand - Body.RightShoulder;
        Vector3 leftHandPosition = leftHandDelta * armScale + Body.LeftShoulder + bodyOffset;
        Vector3 rightHandPosition = rightHandDelta * armScale + Body.RightShoulder + bodyOffset;

        // set position
        LeftHandObject.transform.position = CameraPosition + game.transform.TransformVector(leftHandPosition);
        RightHandObject.transform.position = CameraPosition + game.transform.TransformVector(rightHandPosition);

        // get hand directions
        Vector3 leftHandDirection = game.transform.TransformVector(Body.LeftHand - Body.LeftElbow);
        Vector3 rightHandDirection = game.transform.TransformVector(Body.RightHand - Body.RightElbow);

        // set orientation
        // figure out later
        // TODO
    }
}
