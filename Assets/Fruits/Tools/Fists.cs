﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Default fists, nothing special. Used in menu selection.
 */
[CreateAssetMenu(fileName = "Fists", menuName = "Tools/Fists")]
public class Fists : Tool
{
}
