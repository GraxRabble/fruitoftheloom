﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BoxingGloves", menuName = "Tools/BoxingGloves")]
public class BoxingGloves : Tool
{
    public float PowerLevel = 32;

    /**
     * Uses GoGo Arm Extension to punch objects in the distance. The fist grows
     * larger as it moves further down, maintaning a constant apparent size.
     */
    public override void Loop() {
        Vector3 CameraPosition = Camera.main.transform.position;

        GameManager.BodyState Body = game.Body;

        Vector3 leftHandDelta = GoGoTransform(Body.LeftHand - Body.LeftShoulder, PowerLevel);
        Vector3 rightHandDelta = GoGoTransform(Body.RightHand - Body.RightShoulder, PowerLevel);

        LeftHandObject.transform.position = CameraPosition + game.transform.TransformVector(Body.LeftHand + leftHandDelta + game.bodyOffset);
        RightHandObject.transform.position = CameraPosition + game.transform.TransformVector(Body.RightHand + rightHandDelta + game.bodyOffset);

        // Scale arms to keep same aparent size
        Vector3 leftHandOrigin = LeftHandObject.transform.position - CameraPosition;
        Vector3 rightHandOrigin = RightHandObject.transform.position - CameraPosition;
        LeftHandObject.transform.localScale = new Vector3(1, 1, 1) * leftHandOrigin.magnitude * 0.1f;
        RightHandObject.transform.localScale = new Vector3(1, 1, 1) * rightHandOrigin.magnitude * 0.1f;
    }


    /**
     * Extends a vector by an expoential amount, used in GoGo arms.
     */
    private Vector3 GoGoTransform(Vector3 input, float power)
    {
        float mag = ExponentialTransform(input.magnitude, power) + 1;
        return input.normalized * mag;
    }

    /**
     * Apply Exponential transform to input, Very explosive increases.
     * This is great for small inputs that need large output values.
     */
    private float ExponentialTransform(float input, float power)
    {
        return Mathf.Pow(power, input);
    }
}
