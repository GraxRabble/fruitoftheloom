﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Grapple", menuName = "Tools/Grapple")]
public class Grapple : Tool
{
    // Cooldown is measured in seconds
    private float CoolDown = 0;

    // current object that's being grabbed
    private bool grabbedObject;
    private GameObject originalHand;

    /**
     * Projectile aiming mode
     * Move left hand to target and tap elbow to fire a projectile
     */
    public override void Loop()
    {
        // based on normal arm movement
        base.Loop();

        // Projectile Code
        Vector3 CameraPosition = Camera.main.transform.position;

        // keep ring facing towards camera, used for aiming at targets
        Vector3 aimDirection = LeftHandObject.transform.position - CameraPosition;
        LeftHandObject.transform.rotation = Quaternion.LookRotation(aimDirection);

        GameManager.BodyState Body = game.Body;

        

        /* fire ring projectile through aiming ring */
        if (Vector3.Distance(Body.LeftElbow, Body.RightHand) < 0.3)
        {
            if (CoolDown <= 0)
            {
                Vector3 origin = Camera.main.transform.position;
                Vector3 direction = LeftHandObject.transform.position - origin;

                if (!grabbedObject)
                {
                    RaycastHit[] hits = Physics.SphereCastAll(new Ray(origin, direction), 0.5f, 100);
                    foreach (RaycastHit hit in hits)
                    {
                        if (hit.rigidbody == null)
                            continue;

                        GameObject target = hit.rigidbody.transform.root.gameObject;

                        // ignore player
                        if (target.CompareTag("Player"))
                            continue;

                        // ignore non grabables
                        Grabable g = target.GetComponent<Grabable>();
                        if (g == null)
                            continue;
                        g.Grab();

                        // scale down target
                        Vector3 targetDistance = target.transform.position - origin;
                        target.transform.localScale *= direction.magnitude / targetDistance.magnitude;

                        // replace hand
                        target.transform.position = LeftHandObject.transform.position;
                        LeftHandObject.SetActive(false);
                        originalHand = LeftHandObject;
                        LeftHandObject = target;
                        grabbedObject = true;
                        break;
                    }
                } else
                {
                    // throw object
                    LeftHandObject.GetComponent<Rigidbody>().velocity = direction.normalized * 10;

                    // restore original hand
                    LeftHandObject = originalHand;
                    LeftHandObject.SetActive(true);

                    grabbedObject = false;
                }
                CoolDown = 1;
            }
            CoolDown -= Time.deltaTime;
        }
    }
}
