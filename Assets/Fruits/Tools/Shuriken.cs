﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Shuriken", menuName = "Tools/Shuriken")]
public class Shuriken : Tool
{
    // Projectile used by the shuriken object.
    public GameObject Projectile;

    // Cooldown is measured in seconds
    private float CoolDown = 0;

    /**
     * Projectile aiming mode
     * Move left hand to target and tap elbow to fire a projectile
     */
    public override void Loop()
    {
        // based on normal arm movement
        base.Loop();

        // Projectile Code
        Vector3 CameraPosition = Camera.main.transform.position;

        // keep ring facing towards camera, used for aiming at targets
        Vector3 aimDirection = LeftHandObject.transform.position - CameraPosition;
        LeftHandObject.transform.rotation = Quaternion.LookRotation(aimDirection);

        GameManager.BodyState Body = game.Body;

        /* fire ring projectile through aiming ring */
        if (Vector3.Distance(Body.LeftElbow, Body.RightHand) < 0.3) {
            if (CoolDown <= 0) {
                GameObject proj = Instantiate(Projectile, LeftHandObject.transform.position + aimDirection.normalized * 0.1f, Quaternion.LookRotation(aimDirection));
                Rigidbody rb = proj.GetComponent<Rigidbody>();
                rb.AddForce(aimDirection.normalized * 10, ForceMode.VelocityChange);
                rb.useGravity = false;
                Destroy(proj, 20);
                CoolDown = 1;
            }
            CoolDown -= Time.deltaTime;
        }
    }
}
