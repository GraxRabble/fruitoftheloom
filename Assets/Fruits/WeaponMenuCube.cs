﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponMenuCube : MonoBehaviour {

    public delegate void PickedWeapon(Tool blah);
    public PickedWeapon menuCallback;
    public Tool tool;

    SpriteRenderer render;

    void SetSprite(Sprite sprite)
    {
        // adjust size of the object to always have a constant size, independent of sprite size
        Bounds bounds = sprite.bounds;
        float max = Mathf.Max(bounds.size.x, bounds.size.y, bounds.size.z);
        render.transform.localScale = new Vector3(1, 1, 1) * (1 / max);

        render.sprite = sprite;
    }

    public void Initialize(Tool tool)
    {
        this.tool = tool;
        SetSprite(tool.Sprite);
    }

	// Use this for initialization
	void Awake () {
        render = GetComponentInChildren<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {

        // always face towards camera
        transform.forward = -Camera.main.transform.forward;
	}

    void OnTriggerEnter(Collider other)
    {
        menuCallback(tool);
    }
}
