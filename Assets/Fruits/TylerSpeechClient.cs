﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

public class TylerSpeechClient : MonoBehaviour {

    public int port = 888;
    private UdpClient socket;

    bool giftOfHarambe;

	// Use this for initialization
	void Start () {
        giftOfHarambe = false;

        try {
            // initialize socket
            socket = new UdpClient(port);

            // initialize event handler
            socket.BeginReceive(new AsyncCallback(SpeechMessageRecieved), socket);

        } catch (Exception exception) {
            Debug.LogError(exception);
        }
	}

    // Use this for clean up
    void OnDestroy()
    {
        // close socket
        socket.Close();
    }

    /**
     * Event handler for recieving messages from speech server over UDP port.
     */
    void SpeechMessageRecieved(IAsyncResult result)
    {
        // extract UDP socket from parameter
        UdpClient socket = result.AsyncState as UdpClient;

        // extract message from packet
        IPEndPoint endPoint = new IPEndPoint(0, 0);
        byte[] data = socket.EndReceive(result, ref endPoint);
        string message = Encoding.ASCII.GetString(data);

        // print message
        Debug.Log(message);

        if (message.CompareTo("harambe harambe harambe") == 0)
            giftOfHarambe = true;

        // initialize another event handler for the next message
        socket.BeginReceive(new AsyncCallback(SpeechMessageRecieved), socket);
    }

	// Update is called once per frame
	void Update () {
        if (giftOfHarambe) {
            giftOfHarambe = false;

            GameObject player = GameObject.FindGameObjectWithTag("Player");
            for (int i = 0; i < 10; i++) {
                Vector3 offset = UnityEngine.Random.insideUnitCircle * 5;
                offset = new Vector3(offset.x, 0, offset.y);
                Instantiate(Resources.Load("PickableItem"), player.transform.position + offset, player.transform.rotation);
            }
        }
	}
}
