﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * 3d sprite that's pickable within the game world.
 */
[RequireComponent(typeof(Collider))]
public class RpgPickable : MonoBehaviour {
    public RpgItem item;
    public float WorldSize = 1;

    SpriteRenderer render;

    void SetSprite(Sprite sprite)
    {
        // adjust size of the object to always have a constant size, independent of sprite size
        Bounds bounds = sprite.bounds;
        float max = Mathf.Max(bounds.size.x, bounds.size.y, bounds.size.z);
        render.transform.localScale = new Vector3(1, 1, 1) * (WorldSize / max);

        render.sprite = sprite;
    }

    public void Initialize(RpgItem item)
    {
        this.item = item;
        SetSprite(item.Sprite);
    }

    /**
     * Must use awake so the game will initialize immediately after instantiation.
     */
    void Awake ()
    {
        render = GetComponentInChildren<SpriteRenderer>();
        if (item)
        {
            SetSprite(item.Sprite);
        }
    }

    void Update ()
    {
        // always face towards camera
        transform.forward = -Camera.main.transform.forward;
    }

	void OnTriggerEnter (Collider other)
    {
        // ignore if pickable contains no item
        if (item == null)
            return;

        RpgInventory inventory = other.GetComponentInParent<RpgInventory>();

        if (inventory && inventory.isActiveAndEnabled)
        {
            inventory.AddItem(item);
            Destroy(gameObject);
        }
    }
}
