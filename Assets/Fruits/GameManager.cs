﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(KinectManager))]
[RequireComponent(typeof(KinectController))]
public class GameManager : MonoBehaviour {

    /**
     * Integer values of each corresponding Joint in the Kinect 1.8 SDK.
     * Notice that all joints are integers in the range [0, 19].
     */
    enum JointType
    {
        Groin = 0,
        Belly,
        Neck,
        Head,
        LeftShoulder = 4,
        LeftElbow,
        LeftWrist,
        LeftHand,
        RightShoulder = 8,
        RightElbow,
        RightWrist,
        RightHand,
        LeftHip = 12,
        LeftKnee,
        LeftAnkle,
        LeftFoot,
        RightHip = 16,
        RightKnee,
        RightAnkle,
        RightFoot
    }

    // state of the game
    private StateMachine GameStateMachine;

    // state of the player's tool
    public ToolManager ToolManager;
    public Tool InitialTool;
    public ToolMenuList menuList;

    // gui textbox
    public Text textbox;

    // will be moved into menu later on
    public GameObject weaponCube;

    // offset and scale of the player
    public Vector3 bodyOffset;
    public float armScale;
    public float legScale;

    // speed parameters set by editor
    public float GroundSpeed = 25;
    public float FlightSpeed = 25;
    public float JumpStrength = 5;

    // objects which represent virtual body parts of the player
    // will be moved into weapons later on
    public GameObject leftFootCube;
    public GameObject rightFootCube;

    // references to components
    public KinectManager KinectMan { get; set; }
    public Rigidbody PlayerRBody { get; set; }

    /**
     * Store the position of all body parts detected by Kinect 1.8
     */
    public struct BodyState {
        public Vector3 Groin;
        public Vector3 Belly;
        public Vector3 Neck;
        public Vector3 Head;
        public Vector3 LeftShoulder;
        public Vector3 LeftElbow;
        public Vector3 LeftWrist;
        public Vector3 LeftHand;
        public Vector3 RightShoulder;
        public Vector3 RightElbow;
        public Vector3 RightWrist;
        public Vector3 RightHand;
        public Vector3 LeftHip;
        public Vector3 LeftKnee;
        public Vector3 LeftAnkle;
        public Vector3 LeftFoot;
        public Vector3 RightHip;
        public Vector3 RightKnee;
        public Vector3 RightAnkle;
        public Vector3 RightFoot;
    };

    // Public accessable body state
    public BodyState Body;

    // array of smoothers for each joint
    private DeSmoother[] Smoothers;




    // Use this for initialization
    void Start () {
        KinectMan = GetComponent<KinectManager>();
        PlayerRBody = GetComponent<Rigidbody>();

        // initialize Smoothers
        Smoothers = new DeSmoother[20];
        for (int i = 0; i < Smoothers.Length; i++)
        {
            Smoothers[i] = new DeSmoother();
        }

        // initialize tool manager
        ToolManager = new ToolManager(InitialTool);

        // initialize state machine of the game
        GameStateMachine = new StateMachine();
        GameStateMachine.InitialState(new GamePlay(GameStateMachine, this));
        GameStateMachine.ChangeState(new GamePause(GameStateMachine, this));

        // instanciate feet
        leftFootCube = Instantiate(leftFootCube);
        rightFootCube = Instantiate(rightFootCube);
    }
	
	// Update is called once per frame
	void Update () {
        // update game state
        GameStateMachine.Loop();
    }

    void FixedUpdate ()
    {
    }


    /* --------------------------------------------------------------------------------------------
     *                                  Gesture Detection
     * -------------------------------------------------------------------------------------------- */

    
    
    /**
     * Detect the toolbelt gesture.
     * Both hands in the hip region and near each other.
     */
    public bool DetectToolbelt()
    {
        // detect if hands are near groin
        Vector3 lg = Body.LeftHand - Body.Groin;
        Vector3 rg = Body.RightHand - Body.Groin;
        Vector3 box = new Vector3(0.5f, 0.2f, 0.2f);

        // detect if hands are near each other
        float handDistance = Vector3.Distance(lg, rg);

        return DetectBox(lg, box) && DetectBox(rg, box) && handDistance < 0.3f;
    }

    /**
     * Detect the flying gesture.
     * Both hands above head.
     */
    public bool DetectFly()
    {
        // calculate length normalized arm from shoulder to hand, left arm
        Vector3 lg1 = Body.LeftElbow - Body.LeftShoulder;
        Vector3 lg2 = Body.LeftHand - Body.LeftElbow;
        Vector3 lg = lg1.normalized + lg2.normalized;

        // calculate length normalized arm from shoulder to hand, right arm
        Vector3 rg1 = Body.RightElbow - Body.RightShoulder;
        Vector3 rg2 = Body.RightHand - Body.RightElbow;
        Vector3 rg = rg1.normalized + rg2.normalized;

        return lg.y > 1.8f && rg.y > 1.8f;
    }

    /**
     * Detect the crouching gesture.
     * Squatting or both feet near groin.
     */
    public bool DetectCrouch()
    {
        // calculate length normalized leg from hip to foot, left leg
        Vector3 lg1 = Body.LeftKnee - Body.LeftHip;
        Vector3 lg2 = Body.LeftFoot - Body.LeftKnee;
        Vector3 lg = lg1.normalized + lg2.normalized;

        // calculate length normalized leg from hip to foot, right leg
        Vector3 rg1 = Body.RightKnee - Body.RightHip;
        Vector3 rg2 = Body.RightFoot - Body.RightKnee;
        Vector3 rg = rg1.normalized + rg2.normalized;

        return lg.y > -1.5f && rg.y > -1.5f;
    }

    /**
     * Detect the jumping gesture.
     * Large vertical body movement.
     */
    Vector3 oldGroin;
    public Vector3 DeltaGroin;
    public bool DetectJump()
    {
        DeltaGroin = Body.Groin - oldGroin;
        oldGroin = Body.Groin;

        return DeltaGroin.y > 0.02f;
    }

    /**
     * Detect if the vector is inside of a symmetric box. You must center
     * the input vector onto a body part before passing it to this function.
     */
    private bool DetectBox(Vector3 i, Vector3 b)
    {
        return Mathf.Abs(i.x) < b.x && Mathf.Abs(i.y) < b.y && Mathf.Abs(i.z) < b.z;
    }

    /* --------------------------------------------------------------------------------------------
     *                              Kinect Input Polling
     * -------------------------------------------------------------------------------------------- */

    /**
     * Read joint information from Kinect
     */
    public void PollJoints()
    {
        // torso/spine
        Body.Groin = PollJoint(JointType.Groin);
        Body.Belly = PollJoint(JointType.Belly);
        Body.Neck = PollJoint(JointType.Neck);
        Body.Head = PollJoint(JointType.Head);

        // arms
        Body.LeftShoulder = PollJoint(JointType.LeftShoulder);
        Body.LeftElbow = PollJoint(JointType.LeftElbow);
        Body.LeftWrist = PollJoint(JointType.LeftWrist);
        Body.LeftHand = PollJoint(JointType.LeftHand);
        Body.RightShoulder = PollJoint(JointType.RightShoulder);
        Body.RightElbow = PollJoint(JointType.RightElbow);
        Body.RightWrist = PollJoint(JointType.RightWrist);
        Body.RightHand = PollJoint(JointType.RightHand);

        // legs
        Body.LeftHip = PollJoint(JointType.LeftHip);
        Body.LeftKnee = PollJoint(JointType.LeftKnee);
        Body.LeftAnkle = PollJoint(JointType.LeftAnkle);
        Body.LeftFoot = PollJoint(JointType.LeftFoot);
        Body.RightHip = PollJoint(JointType.RightHip);
        Body.RightKnee = PollJoint(JointType.RightKnee);
        Body.RightAnkle = PollJoint(JointType.RightAnkle);
        Body.RightFoot = PollJoint(JointType.RightFoot);
    }

    /**
     * Poll some Joint Data from the Kinect.
     * This function will perform all filtering and transformations into Unity Space.
     */
    private Vector3 PollJoint(JointType joint)
    {
        Vector3 raw = KinectMan.GetJointPosition(KinectMan.GetPlayer1ID(), (int)joint);
        Vector3 smoothed = Smoothers[(int)joint].get(raw);
        return MapIntoUnitySpace(smoothed);
    }

    /**
     * Convert a 3d coordinate stored in Kinect Space into Unity Space.
     * 
     * Kinect uses XY as the horizontal plane and Z as the up axis.
     * Unity uses XZ as the horizontal plane asn Y as the up axis.
     * 
     * Moving towards the Kinect is considered negative when it should be postive.
     */
    private Vector3 MapIntoUnitySpace(Vector3 input)
    {
        return new Vector3(input.x, input.y, -input.z);
    }

    /* --------------------------------------------------------------------------------------------
     *                                      Body Movement
     * -------------------------------------------------------------------------------------------- */

    /**
     * Handle basic movement involving leaning
     */
    public void LeaningMovement()
    {
        // body movement code - experimental
        // create a vector from groin to head, this is our lean vector.
        Vector3 lean = Body.Head - Body.Groin;

        // how much we are leaning 'forward'
        float forward = CutoffTransform(lean.z, 0.1f);
        // how much we are leaning 'sideways'
        float strafe = CutoffTransform(lean.x, 0.1f);

        // performs rotation depending on the orientation of the hip relative to the kinect
        // hip vector
        Vector3 bodyRight = Body.RightHip - Body.LeftHip;
        // normal vector
        Vector3 bodyUp = Body.Head - Body.Groin;
        // imiginary kinect vector to compare hips against
        Vector3 kinectRight = new Vector3(1, 0, 0);
        float angle = Vector3.Angle(bodyRight, kinectRight);
        float sign = Mathf.Sign(Vector3.Dot(bodyUp, Vector3.Cross(kinectRight, bodyRight)));

        // how much are the hips rotated
        float bodyRotation = sign * ParabolaTransform(CutoffTransform(angle, 15), 0.5f);


        // perform actual movement
        transform.Rotate(0, bodyRotation * 15 * Time.deltaTime, 0);
        transform.Translate(strafe * GroundSpeed * Time.deltaTime, 0, forward * GroundSpeed * Time.deltaTime);
    }

    /**
     * Handle movement involving flying
     */
    public void FlyingMovement()
    {
        // calculate length normalized arm from shoulder to hand, left arm
        Vector3 lg1 = Body.LeftElbow - Body.LeftShoulder;
        Vector3 lg2 = Body.LeftHand - Body.LeftElbow;
        Vector3 lg = lg1.normalized + lg2.normalized;
        lg /= 2;

        // calculate length normalized arm from shoulder to hand, right arm
        Vector3 rg1 = Body.RightElbow - Body.RightShoulder;
        Vector3 rg2 = Body.RightHand - Body.RightElbow;
        Vector3 rg = rg1.normalized + rg2.normalized;
        rg /= 2;

        // average magnitude of arm extension
        float am = ParabolaTransform((lg.magnitude + rg.magnitude) * 0.5f, 4);

        // move forward at a speed porportional to arm extension
        // fully extended, full speed
        transform.Translate(0, 0, am * FlightSpeed * Time.deltaTime);

        // average position of arms
        Vector3 hv = (rg + lg) * 0.5f;

        /*
         * rotate body along all three axis.
         * First axis uses the height of the average hand position.
         *      Move both hands up to go up, both hands down to go down.
         * Second and third axis uses bar metaphor.
         *      Imagine holding a bar
         *          rotating left to right changes yaw
         *          rotating up and down changes roll
         */
        transform.Rotate(-hv.y, -(rg - lg).z, (rg - lg).y, Space.Self);
    }

    /* --------------------------------------------------------------------------------------------
     *                                  Bodyparts Movement
     * -------------------------------------------------------------------------------------------- */

    /**
     * Normal Feet Movement
     */
    public void NormalFeet()
    {
        Vector3 CameraPosition = Camera.main.transform.position;

        // move foot cubes
        Vector3 leftFootDelta = Body.LeftFoot - Body.LeftHip;
        Vector3 rightFootDelta = Body.RightFoot - Body.RightHip;
        Vector3 leftFootPosition = leftFootDelta * legScale + Body.LeftHip + bodyOffset;
        Vector3 rightFootPosition = rightFootDelta * legScale + Body.RightHip + bodyOffset;

        leftFootCube.transform.position = CameraPosition + transform.TransformVector(leftFootPosition);
        rightFootCube.transform.position = CameraPosition + transform.TransformVector(rightFootPosition);
    }

    /* --------------------------------------------------------------------------------------------
     *                              Math Transforms
     * -------------------------------------------------------------------------------------------- */

    /**
     * Extends a vector by an expoential amount, used in GoGo arms.
     */
    private Vector3 GoGoTransform(Vector3 input, float power)
    {
        float mag = ExponentialTransform(input.magnitude, power) + 1;
        return input.normalized * mag;
    }

    /**
     * Apply Parabolic transformation to input, Good for moderate inceases.
     */
    private float ParabolaTransform(float input, float power)
    {
        return Mathf.Sign(input) * Mathf.Pow(input, power);
    }

    /**
     * Apply Exponential transform to input, Very explosive increases.
     * This is great for small inputs that need large output values.
     */
    private float ExponentialTransform(float input, float power)
    {
        return Mathf.Pow(power, input);
    }

    /**
     * Apply a cutoff to the input, Forces all values below to be 0.
     * This is great for adding a deadzone for movement.
     */
    private float CutoffTransform(float input, float threshold)
    {
        if (Mathf.Abs(input) > threshold) {
            return input - threshold;
        } else {
            return 0;
        }
    }

    /**
     * Apply a logorithmic transform, Slows down the increase of a value.
     * This is great for adding a smooth cap to a value.
     */
    private float LogTransform(float input, float power)
    {
        return Mathf.Log(input, power);
    }
}
