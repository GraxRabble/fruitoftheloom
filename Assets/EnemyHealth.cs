﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    public float health;
    public AudioClip deathSound;

    EnemyMovement em;
    EnemyAnimation ea;
    RpgInventory inventory;
    AudioSource sound;

    bool dead;

	// Use this for initialization
	void Start () {
        dead = false;
        em = GetComponent<EnemyMovement>();
        ea = GetComponent<EnemyAnimation>();
        inventory = GetComponent<RpgInventory>();
        sound = GetComponent<AudioSource>();
    }

    public void Damage(float damage)
    {
        health -= damage;
        if (health < 0)
        {
            Die();
        }
    }
    
    void Die()
    {
        // you can only die once
        if (dead) return;
        dead = true;

        // disable everything AI releated if the enemy has it
        if (em)
            em.enabled = false;
        if (ea)
            ea.enabled = false;
        if (inventory)
        {
            inventory.DropItems();
            inventory.enabled = false;
        }

        if (sound)
            sound.PlayOneShot(deathSound);

        Destroy(gameObject, 30);
    }
}
