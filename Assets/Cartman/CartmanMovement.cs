﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartmanMovement : MonoBehaviour {

    Animator anim;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();

        anim.SetFloat("Forward", 5.0f);
        anim.SetFloat("Strafe", 1.0f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
