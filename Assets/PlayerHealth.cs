﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    public float health = 100;
    public Text healthText;

    bool dead;

	// Use this for initialization
	void Start () {
        dead = false;
        healthText.text = health.ToString();
	}
	
	// Update is called once per frame
	void Update () {
        if (health > 100) {
            health *= 0.9999f;
        }
	}

    public void DamagePlayer(float damage)
    {
        health -= damage;
        healthText.text = health.ToString();
        if (health < 0) {
            Die();
        }
    }

    void Die()
    {
        // yolo
        if (!dead)
            dead = true;

        // you die stuff here
        healthText.text = "You Died!";
    }
}
