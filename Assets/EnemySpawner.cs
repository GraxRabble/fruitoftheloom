﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
    public GameObject enemy;
    public float SpawnTime = 20;

    float Timer;
    // Use this for initialization
    void Start()
    {
        Timer = SpawnTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (Timer < 0) {
            Timer = SpawnTime;

            SpawnEnemy();
        }
        Timer -= Time.deltaTime;
    }

    void SpawnEnemy()
    {
        Instantiate(enemy, transform.position, transform.rotation);
    }
}
