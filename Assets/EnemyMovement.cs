﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyMovement : MonoBehaviour {

    public NavMeshAgent agent { get; set; }
    public Animator animator { get; set; }
    public float SightDistance = 255;
    public float SightAngle = 30;
    public float ChaseTime = 5;
    public Transform[] PatrolPoints;

    StateMachine state;

	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        state = new StateMachine();
        state.InitialState(new EnemyIdle(state, this));
	}
	
	// Update is called once per frame
	void Update () {
        state.Loop();
    }

    void OnDisable()
    {
        agent.enabled = false;
    }

    /**
     * Line of sight detection
     */
    public bool LineOfSightCheck(Vector3 target)
    {
        Vector3 forward = transform.forward;
        Vector3 origin = transform.position;
        Vector3 direction = target - origin;
        Ray raycast = new Ray(origin, direction);

        // ray found something
        RaycastHit hit;
        if (Physics.Raycast(raycast, out hit, SightDistance)) {
            // check if found object was the player
            if (hit.collider.gameObject.CompareTag("Player")) {
                // check if player is within view cone
                if (Vector3.Dot(forward.normalized, direction.normalized) > Mathf.Cos(SightAngle * Mathf.PI / 180)) {
                    return true;
                }
            }
        }
        return false;
    }
}


internal class EnemyIdle : State
{
    private EnemyMovement e;

    public EnemyIdle(StateMachine manager, EnemyMovement e) : base(manager)
    {
        this.e = e;
    }

    public override void Enter()
    {
    }

    public override void Exit()
    {
    }

    public override void Loop()
    {
        if (e.agent.isOnNavMesh)
        {
            int choice = UnityEngine.Random.Range(0, 3);
            switch (choice) {
                case 0:
                    NextState(new EnemyWait(manager, e));
                    break;
                case 1:
                    NextState(new EnemyEat(manager, e));
                    break;
                case 2:
                    NextState(new EnemyPatrol(manager, e));
                    break;
            }
        }
    }
}

internal class EnemyWait : State
{
    private EnemyMovement e;

    float WaitTimer = 10;

    public EnemyWait(StateMachine manager, EnemyMovement e)
        : base(manager)
    {
        this.e = e;
    }

    public override void Enter()
    {
    }

    public override void Exit()
    {
    }

    public override void Loop()
    {
        if (!e.agent.isOnNavMesh) {
            NextState(new EnemyIdle(manager, e));
            return;
        }

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        Vector3 target = player.transform.position;
        if (e.LineOfSightCheck(target))
            NextState(new EnemyChase(manager, e));

        // tired of waiting for player, so do something else, like eat fruit
        if (WaitTimer < 0)
            NextState(new EnemyIdle(manager, e));
        WaitTimer -= Time.deltaTime;
    }
}

internal class EnemyPatrol : State
{
    private EnemyMovement e;

    Transform target;

    public EnemyPatrol(StateMachine manager, EnemyMovement e)
        : base(manager)
    {
        this.e = e;
    }

    public override void Enter()
    {
        int choice = UnityEngine.Random.Range(0, e.PatrolPoints.Length);
        target = e.PatrolPoints[choice];
        if (target == null) {
            GameObject.Destroy(e.gameObject);
            return;
        }
        e.agent.destination = target.position;
    }

    public override void Exit()
    {
    }

    public override void Loop()
    {
        if (!e.agent.isOnNavMesh) {
            NextState(new EnemyIdle(manager, e));
            return;
        }

        // look for player along patrol point
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        Vector3 target = player.transform.position;
        if (e.LineOfSightCheck(target))
            NextState(new EnemyChase(manager, e));

        // if reach patrol point, stop
        if (e.agent.remainingDistance < 1)
            NextState(new EnemyIdle(manager, e));
    }
}

internal class EnemyChase : State
{
    private EnemyMovement e;

    float ChaseTimer;

    public EnemyChase(StateMachine manager, EnemyMovement e) : base(manager)
    {
        this.e = e;
        ChaseTimer = e.ChaseTime;
    }

    public override void Enter()
    {
    }

    public override void Exit()
    {
    }

    public override void Loop()
    {
        if (!e.agent.isOnNavMesh)
        {
            NextState(new EnemyIdle(manager, e));
            return;
        }


        GameObject player = GameObject.FindGameObjectWithTag("Player");
        Vector3 target = player.transform.position;
        if (e.LineOfSightCheck(target)) {
            // update player's position as long as the enemy can see him
            Vector3 playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
            e.agent.SetDestination(playerPosition);
        } else {
            // if cannot see player, start timer to forget player
            if (ChaseTimer < 0) {
                NextState(new EnemyWait(manager, e));
            }
            ChaseTimer -= Time.deltaTime;
        }
    }
}

internal class EnemyEat : State
{
    private EnemyMovement e;
    RpgPickable target;

    public EnemyEat(StateMachine manager, EnemyMovement e)
        : base(manager)
    {
        this.e = e;
    }

    public override void Enter()
    {
        target = GameObject.FindObjectOfType<RpgPickable>();
    }

    public override void Exit()
    {
    }

    public override void Loop()
    {
        if (!e.agent.isOnNavMesh) {
            NextState(new EnemyIdle(manager, e));
            return;
        }

        if (target)
            e.agent.destination = target.transform.position;
        else
            NextState(new EnemyIdle(manager, e));
    }
}