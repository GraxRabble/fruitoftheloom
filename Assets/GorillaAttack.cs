﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GorillaAttack : MonoBehaviour
{

    public float DamageAmount = 10;
    public float PushStrength = 10;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) {
            // do damage to player
            PlayerHealth ph = other.GetComponent<PlayerHealth>();
            if (ph)
                ph.DamagePlayer(DamageAmount);
        }
    }
}
