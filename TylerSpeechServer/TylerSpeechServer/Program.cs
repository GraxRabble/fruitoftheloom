﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Speech.Recognition;
using System.Net;
using System.Net.Sockets;

class Program
{
    static bool running;

    static int port = 888;

    static UdpClient socket;

    static void Main(string[] args)
    {
        // turn on speech recognizer and use default microphone
        SpeechRecognitionEngine speech;
        speech = new SpeechRecognitionEngine(new System.Globalization.CultureInfo("en-US"));
        speech.SetInputToDefaultAudioDevice();

        // detect some keywords
        GrammarBuilder gb = new GrammarBuilder();
        gb.Append("harambe");
        gb.Append("harambe");
        gb.Append("harambe");
        Grammar g = new Grammar(gb);
        speech.LoadGrammar(g);

        // attach event handler for async speech detection
        speech.SpeechRecognized += SpeechFound;

        // initalize UDP socket
        try {
            socket = new UdpClient();
        } catch (Exception exception) {
            Console.WriteLine(exception);
        }

        // run in loop until quit
        running = true;
        while (running) {
            speech.Recognize();
        }

        // close socket
        socket.Close();
    }

    /**
     * Speech handling function used by seperate thread for async detection.
     */
    private static void SpeechFound(object sender, SpeechRecognizedEventArgs e)
    {
        // get message from speech recognizer
        string message = e.Result.Text;
        float confidence = e.Result.Confidence;

        // send to console
        Console.WriteLine("{0} : {1}", confidence, message);

        // send message to unity
        try {
            byte[] data = Encoding.ASCII.GetBytes(message);
            IPEndPoint target = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
            socket.Send(data, data.Length, target);
        } catch (Exception exception) {
            Console.WriteLine(exception);
        }

        // if quit, close application
        if (e.Result.Text == "quit") {
            running = false;
        }
    }
}
